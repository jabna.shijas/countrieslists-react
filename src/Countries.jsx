import React, { useState, useEffect } from 'react';

const Countries = () => { 
  const [countries, setCountries] = useState([]); 
  const [search, setSearch] = useState(''); 
  const [filteredCountries, setFilteredCountries] = useState([]); 

  useEffect(() => { 
    fetch('https://restcountries.com/v3.1/all') 
      .then(response => response.json())
      .then(data => {
        setCountries(data); 
        setFilteredCountries(data);
      })
      .catch(error => {
        console.error('Error fetching countries:', error);
      }); 
  }, []); // Empty dependency array means this effect runs once after the initial render

  const handleInputChange = (event) => {
    setSearch(event.target.value); // Update the state with the new input value
  };

  const handleSearch = () => {
    const filtered = countries.filter(country => 
      country.name.common.toLowerCase().includes(search.toLowerCase())
    );
    setFilteredCountries(filtered); // Update the filteredCountries state with the filtered list
  };

  return (
    <div>
      <h1>Countries</h1> 
      <div>
        <input type="text"  value={search} // Bind the input value to the state variable
          onChange={handleInputChange} // Use the handler function for the onChange event
          placeholder="Search the country" // Placeholder text for the input field
        />
        <button onClick={handleSearch}>Search</button> 
      </div>
      <ul>
        {filteredCountries.map(country => ( 
          <li key={country.cca3}> 
            <img
              src={country.flags.png} 
            
              style={{ width: '50px', marginRight: '10px' }} 
            />
            {country.name.common} 
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Countries; 
